import { Container } from "@mui/material";
import StartForm from "./StartForm/StartForm";
import ResultForm from "./ResultForm/ResultForm";
import BackdropLoading from "./BackdropLoading/BackdropLoading";
import { useSelector } from "react-redux";

const WeatherForecastForm = () => {
    const {loadingData, changeForm} = useSelector(reduxData => reduxData.weatherForecastReducer);
    return (
        <Container>
            {loadingData ? <BackdropLoading /> : changeForm ? <ResultForm /> : <StartForm />}
        </Container>
    )
}

export default WeatherForecastForm;