import { Alert, Card, CardContent, CardMedia, Grid, Snackbar, Typography } from "@mui/material";
import weatherPic from "../../assets/images/01d.png";
import InputField from "../InputField/InputField";
import { useDispatch, useSelector } from "react-redux";
import { closeErrorAlertAction } from "../../actions/weatherForecast.action";

const titleStyle = {
    padding: "70px 50px",
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderRadius: 8,
}


const StartForm = () => {
    const dispatch = useDispatch()
    const { isError, errorMessage } = useSelector(reduxData => reduxData.weatherForecastReducer);

    const closeErrorAlert = () => {
        dispatch(closeErrorAlertAction());
    }
    return (
        <Grid container sx={{height: "100vh"}} justifyContent="center" alignItems="center">
            <Grid item xs={9}>
                <Card sx={titleStyle}>
                    <CardContent>
                        <Grid container alignItems="center" spacing={5}>
                            <Grid item xs={3}>
                                <CardMedia component="img" src={weatherPic} alt="Weather-Forecast" />
                            </Grid>
                            <Grid item xs={9} textAlign="center">
                                <Typography sx={{color: "#365A7A"}} variant="h2" fontWeight={600}>Weather Forecast</Typography>
                            </Grid>
                        </Grid>
                        <InputField />
                    </CardContent>
                </Card>
            </Grid>
            <Snackbar open={isError} autoHideDuration={5000} onClose={closeErrorAlert}>
                <Alert onClose={closeErrorAlert} variant="filled" severity="error" sx={{ width: '100%' }}>
                    {errorMessage}
                </Alert>
            </Snackbar>
        </Grid>
    )
}

export default StartForm;