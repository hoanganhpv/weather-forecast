import { Alert, Box, Card, CardContent, CardMedia, Grid, Snackbar, Typography } from "@mui/material";
import InputField from "../InputField/InputField";
import { useDispatch, useSelector } from "react-redux";
import { closeErrorAlertAction } from "../../actions/weatherForecast.action";

const cardStyle = {
    padding: "70px 50px",
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderRadius: 8,
    height: "350px"
}

const resultCard = {
    padding: "0 20px",
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderRadius: 8,
    textAlign: "center"
}

const ResultForm = () => {
    const dispatch = useDispatch()
    const { weatherData, curentWeather, isError, errorMessage } = useSelector(reduxData => reduxData.weatherForecastReducer);

    const closeErrorAlert = () => {
        dispatch(closeErrorAlertAction());
    }

    return (
        <Grid container sx={{ height: "100vh" }} justifyContent="center" alignItems="center">
            <Grid item xs={9}>
                <Box position="relative">
                    <Card sx={cardStyle}>
                        <CardContent>
                            <Grid container alignItems="center" spacing={10}>
                                <Grid item xs={12} md={5}>
                                    <CardMedia component="img" image={require(`../../assets/images/${curentWeather.weather[0].icon}.png`)} alt="Weather-Forecast" />
                                </Grid>
                                <Grid item xs={12} md={7}>
                                    <Typography variant="h4">Today</Typography>
                                    <Box mt={2}>
                                        <Typography variant="h3"><b>{curentWeather.name}</b></Typography>
                                    </Box>
                                    <Box mt={4}>
                                        <Typography variant="h4">Temperature: {Math.round(curentWeather.main.temp)}°C</Typography>
                                        <Typography variant="h4">{curentWeather.weather[0].description}</Typography>
                                    </Box>
                                </Grid>
                            </Grid>
                        </CardContent>
                        <Box className="search-box" >
                            <InputField />
                        </Box>
                        <Box className="result-box">
                            <Grid container spacing={3}>
                                {weatherData.list.map((element, index) => {
                                    return (
                                        <Grid key={index} item xs={3}>
                                            <Card sx={resultCard}>
                                                <CardContent>
                                                    <Grid item xs={12}>
                                                        <Typography variant="h6">{element.day}</Typography>
                                                        <CardMedia sx={{ width: "100px" }} component="img" image={require(`../../assets/images/${element.weather[0].icon}.png`)} />
                                                        <Typography variant="h5">{Math.round(element.temp.day)}°C</Typography>
                                                    </Grid>
                                                </CardContent>
                                            </Card>
                                        </Grid>
                                    )
                                })}
                            </Grid>
                        </Box>
                    </Card>
                </Box>
            </Grid>
            <Snackbar open={isError} autoHideDuration={5000} onClose={closeErrorAlert}>
                <Alert onClose={closeErrorAlert} variant="filled" severity="error" sx={{ width: '100%' }}>
                    {errorMessage}
                </Alert>
            </Snackbar>
        </Grid>
    )
}

export default ResultForm;