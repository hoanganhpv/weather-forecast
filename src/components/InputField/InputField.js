import { Grid, TextField } from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import { inputChangeAction, searchCityAction } from "../../actions/weatherForecast.action";

const textFieldStyle = {
    marginTop: 5,
    '& .MuiOutlinedInput-root': { borderRadius: '1.5em' },
    '& input': { fontSize: '16px', color: "#365A7A" },
    '& input::placeholder': { fontSize: '16px' },
}

const InputField = () => {
    const dispatch = useDispatch();
    const {inputValue} = useSelector(reduxData => reduxData.weatherForecastReducer);

    const inputChangeHandler = (event) => {
        const value = event.target.value;
        dispatch(inputChangeAction(value));
    }

    const handleSearchCity = (event) => {
        if (event.keyCode === 13) {
            dispatch(searchCityAction(event.target.value));
        }
    }

    return (
        <Grid container justifyContent="center">
            <Grid item xs={7}>
                <TextField sx={textFieldStyle} value={inputValue} onChange={inputChangeHandler} onKeyDown={handleSearchCity} fullWidth placeholder="Enter A City ..." label="City Name" />
            </Grid>
        </Grid>
    )

}

export default InputField