import { combineReducers } from "redux";
import weatherForecastReducer from "./weatherForecast.reducer";

const rootReducer = combineReducers({
    weatherForecastReducer
});

export default rootReducer;