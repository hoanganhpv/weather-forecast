import { CLOSE_ERROR_ALERT, INPUT_CHANGE, LOADING_DATA, LOAD_DATA_ERROR, LOAD_DATA_SUCCESS } from "../constants/weatherForecast.const";

const initialState = {
    inputValue: "",
    curentWeather : [],
    weatherData: [],
    loadingData: false,
    changeForm: false,
    isError: false,
    errorMessage: "City Not Found"
}

const weatherForecastReducer = (state = initialState, action) => {
    switch (action.type) {
        case INPUT_CHANGE:
            state.inputValue = action.payload;
            break;

        case LOADING_DATA:
            state.loadingData = true;
            break;

        case LOAD_DATA_SUCCESS:
            state.inputValue = "";
            state.loadingData = false;
            state.weatherData = action.payload.forecastData;
            state.curentWeather = action.payload.currentData;
            state.changeForm = true;
            break;

        case LOAD_DATA_ERROR:
            state.isError = true;
            state.loadingData = false;
            state.inputValue = "";
            break;

        case CLOSE_ERROR_ALERT: 
            state.isError = false;
            break;

        default: 
            break
    }
    return {...state}
}

export default weatherForecastReducer;