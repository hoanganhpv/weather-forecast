import { CLOSE_ERROR_ALERT, INPUT_CHANGE, LOADING_DATA, LOAD_DATA_ERROR, LOAD_DATA_SUCCESS } from "../constants/weatherForecast.const"

export const inputChangeAction = (value) => {
    return {
        type: INPUT_CHANGE,
        payload: value
    }
}

export const searchCityAction = (value) => {
    return async (dispatch) => {
        try {
            await dispatch({
                type: LOADING_DATA
            })

            const forecastResponse = await fetch("http://api.openweathermap.org/data/2.5/forecast/daily?q=" + value + "&units=metric&cnt=4&APPID=c10bb3bd22f90d636baa008b1529ee25");
            const forecastData = await forecastResponse.json();

            const currentResponse = await fetch('http://api.openweathermap.org/data/2.5/weather?q=' + value + "&units=metric&APPID=c10bb3bd22f90d636baa008b1529ee25")
            const currentData = await currentResponse.json();

            if (forecastResponse.ok && currentResponse.ok) {
                const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                const result = [];
                for (let i = 0; i < 4; i++) {
                    const date = new Date();
                    date.setDate(date.getDate() + i + 1);
                    const dayOfWeek = date.getDay();
                    result.push(days[dayOfWeek]);
                    forecastData.list[i].day = days[dayOfWeek];
                }

                return dispatch({
                    type: LOAD_DATA_SUCCESS,
                    payload: {
                        forecastData,
                        currentData
                    }
                })
            }
            else {
                return dispatch({
                    type: LOAD_DATA_ERROR,
                })
            }
        }
        catch (error) {
            return dispatch({
                type: LOAD_DATA_ERROR,
            })
        }
    }
}

export const closeErrorAlertAction = () => {
    return {
        type: CLOSE_ERROR_ALERT
    }
}