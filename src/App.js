import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import './App.css';
import WeatherForecastForm from './components/WetherForecastForm';

function App() {
  return (
    <div>
      <WeatherForecastForm />
    </div>
  );
}

export default App;